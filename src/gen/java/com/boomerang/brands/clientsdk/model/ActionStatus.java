package com.boomerang.brands.clientsdk.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ActionStatus
 */
public enum ActionStatus {
  INCOMPLETE("in progress"),
    COMPLETE("successful"),
    ERROR("error"),
    IMPOSSIBLE("not possible");

  private String value;

  ActionStatus(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ActionStatus fromValue(String text) {
    for (ActionStatus b : ActionStatus.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }

}
