package com.boomerang.brands.clientsdk.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ActionType
 */
public enum ActionType {
    ADDKEYWORDTOCAMPAIGNS("addKeywordToCampaigns"),
    REMOVEKEYWORD("removeKeyword"),
    UPDATEKEYWORDSTATE("updateKeywordState"),
    KEYWORDBIDCHANGE("keywordBidChange"),
    CAMPAIGNCREATION("CampaignCreation"),
    CAMPAIGNBUDGETCHANGE("campaignBudgetChange"),
    CAMPAIGNSTATUSCHANGE ("campaignStatusChange"),
    ADDNEGATIVEKEYWORD("addNegativeKeyword"),
    UPDATENEGATIVEKEYWORDSTATE("updateNegativeKeywordState"),
    REMOVENEGATIVEKEYWORD("removeNegativeKeyword"),
    ADDASINCAMPAIGN("addAsinCampaign"),
    REMOVEASINCAMPAIGN("removeAsinCampaign"),
    ASINUPDATE("productAdStatusChange"),
    SWAPASINCAMPAIGN("swapAsinCampaign");

    private String value;

    ActionType(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ActionType fromValue(String text) {
        for (ActionType b : ActionType.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
