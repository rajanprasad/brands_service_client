package com.boomerang.brands.clientsdk.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum MatchType {
    BROAD("broad"),
    EXACT("exact"),
    PHRASE("phrase");

    private String value;

    MatchType(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static MatchType fromValue(String text) {
        for (MatchType b : MatchType.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}
