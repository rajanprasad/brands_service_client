package com.boomerang.brands.clientsdk.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class ActionLoggerResp  {

    @JsonProperty("primaryKey")
    private String primaryKey = null;

    @JsonProperty("entityId")
    private String entityId = null;

    @JsonProperty("actionType")
    private ActionType actionType = null;

    @JsonProperty("actionStatus")
    private ActionStatus actionStatus = null;

    @JsonProperty("timeStamp")
    private String timeStamp = null;

    @JsonProperty("username")
    private String userName = null;

    @JsonProperty("actionId")
    private Long actionId = null;

    @JsonProperty("viewPayload")
    private Map<String, Object> viewPayload = null;

    @JsonProperty("actionSource")
    private Map<String, Object> actionSource = null;


    public ActionLoggerResp primaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }

    /**
     * Get primaryKey
     *
     * @return primaryKey
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public ActionLoggerResp entityId(String entityId) {
        this.entityId = entityId;
        return this;
    }

    /**
     * Get primaryKey
     *
     * @return primaryKey
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }


    public ActionLoggerResp actionType(ActionType actionType) {
        this.actionType = actionType;
        return this;
    }

    /**
     * Get actionType
     *
     * @return actionType
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public ActionLoggerResp actionStatus(ActionStatus actionStatus) {
        this.actionStatus = actionStatus;
        return this;
    }

    /**
     * Get actionStatus
     *
     * @return actionStatus
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public ActionStatus getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(ActionStatus actionStatus) {
        this.actionStatus = actionStatus;
    }

    /**
     * Get timeStamp
     *
     * @return timeStamp
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp.toString();
    }
    public ActionLoggerResp userName(String userId) {
        this.userName = userId;
        return this;
    }

    /**
     * Get userName
     *
     * @return userName
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ActionLoggerResp actionId(Long actionId) {
        this.actionId = actionId;
        return this;
    }

    /**
     * Get actionId
     *
     * @return actionId
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ActionLoggerResp actionLoggerResp = (ActionLoggerResp) o;
        return Objects.equals(this.primaryKey, actionLoggerResp.primaryKey) &&
                Objects.equals(this.actionType, actionLoggerResp.actionType) &&
                Objects.equals(this.actionStatus, actionLoggerResp.actionStatus) &&
                Objects.equals(this.timeStamp, actionLoggerResp.timeStamp) &&
                Objects.equals(this.userName, actionLoggerResp.userName) &&
                Objects.equals(this.entityId, actionLoggerResp.entityId) &&
                Objects.equals(this.actionId, actionLoggerResp.actionId);
    }

    public ActionLoggerResp viewPayload(final Map<String, Object> viewPayload) {
        this.viewPayload = viewPayload;
        return this;
    }

    /**
     * Get viewPayload
     *
     * @return viewPayload
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public Map<String, Object> getActionSource() {
        return actionSource;
    }

    public void setActionSource(final Map<String, Object> actionSource) {
        this.actionSource = actionSource;
    }

    public ActionLoggerResp actionSource(final Map<String, Object> actionSource) {
        this.actionSource = actionSource;
        return this;
    }

    /**
     * Get viewType
     *
     * @return viewType
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public Map<String, Object> getViewPayload() {
        return viewPayload;
    }

    public void setViewPayload(final Map<String, Object> viewPayload) {
        this.viewPayload = viewPayload;
    }

    @Override
    public int hashCode() {
        return Objects.hash(primaryKey, actionType, actionStatus, timeStamp, userName, actionId);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ActionLoggerResp {\n");

        sb.append("    primaryKey: ").append(toIndentedString(primaryKey)).append("\n");
        sb.append("    entityId: ").append(toIndentedString(entityId)).append("\n");
        sb.append("    actionType: ").append(toIndentedString(actionType)).append("\n");
        sb.append("    actionStatus: ").append(toIndentedString(actionStatus)).append("\n");
        sb.append("    timeStamp: ").append(toIndentedString(timeStamp)).append("\n");
        sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
        sb.append("    actionId: ").append(toIndentedString(actionId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

