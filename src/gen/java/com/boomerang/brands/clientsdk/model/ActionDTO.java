package com.boomerang.brands.clientsdk.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class ActionDTO {
    @JsonProperty("clientId")
    private Integer clientId = null;

    @JsonProperty("profileId")
    private Long profileId = null;

    @JsonProperty("campaignId")
    private Long campaignId = null;

    @JsonProperty("newBudget")
    private Double newBudget;

    @JsonProperty("previousBudget")
    private Double previousBudget;

    @JsonProperty("newStatus")
    private Status newStatus = null;

    @JsonProperty("adgroupId")
    private Long adgroupId = null;

    @JsonProperty("keywordId")
    private Long keywordId = null;

    @JsonProperty("adId")
    private Long adId = null;

    @JsonProperty("previousStatus")
    private Status previousStatus = null;

    @JsonProperty("newBid")
    private Double newBid;

    @JsonProperty("previousBid")
    private Double previousBid;


    @JsonProperty("sku")
    private String sku = null;

    @JsonProperty("asin")
    private String asin = null;

    @JsonProperty("keywordText")
    private String keywordText = null;

    @JsonProperty("matchType")
    private MatchType matchType = null;

    @JsonProperty("campaignType")
    private String campaignType;

    public ActionDTO newStatus(Status newStatus) {
        this.newStatus = newStatus;
        return this;
    }

    /*** Get newStatus* @return newStatus**/
    @ApiModelProperty(value = "")
    public Status getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(Status newStatus) {
        this.newStatus = newStatus;
    }


    public ActionDTO previousBudget(Double previousBudget) {
        this.previousBudget = previousBudget;
        return this;
    }

    /*** Get previousBudget* @return previousBudget**/
    @ApiModelProperty(value = "")
    public Double getPreviousBudget() {
        return previousBudget;
    }

    public void setPreviousBudget(Double previousBudget) {
        this.previousBudget = previousBudget;
    }

    public ActionDTO newBudget(Double newBudget) {
        this.newBudget = newBudget;
        return this;
    }

    /*** Get newBudget* @return newBudget**/
    @ApiModelProperty(value = "")
    public Double getNewBudget() {
        return newBudget;
    }

    public void setNewBudget(Double newBudget) {
        this.newBudget = newBudget;
    }


    public ActionDTO campaignId(String campaignId) {
        this.campaignId = Long.parseLong(campaignId);
        return this;
    }

    /*** Get campaignId* @return campaignId**/
    @ApiModelProperty(value = "")
    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = Long.parseLong(campaignId);
    }

    public ActionDTO clientId(Integer clientId) {
        this.clientId = clientId;
        return this;
    }

    /**
     * Get clientId
     *
     * @return clientId
     **/
    @ApiModelProperty(value = "")

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public ActionDTO profileId(String profileId) {
        this.profileId = Long.parseLong(profileId);
        return this;
    }

    /**
     * Get profileId
     *
     * @return profileId
     **/
    @ApiModelProperty(value = "")

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = Long.parseLong(profileId);
    }

    public ActionDTO adgroupId(String adgroupId) {
        this.adgroupId = Long.parseLong(adgroupId);
        return this;
    }

    /**
     * Get adgroupId
     *
     * @return adgroupId
     **/
    @ApiModelProperty(value = "")

    public Long getAdgroupId() {
        return adgroupId;
    }

    public void setAdgroupId(String adgroupId) {
        this.adgroupId = Long.parseLong(adgroupId);
    }

    public ActionDTO keywordId(String keywordId) {
        this.keywordId = Long.parseLong(keywordId);
        return this;
    }

    /**
     * Get keywordId
     *
     * @return keywordId
     **/
    @ApiModelProperty(value = "")

    public Long getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(String keywordId) {
        this.keywordId = Long.parseLong(keywordId);
    }

    public ActionDTO adId(String adId) {
        this.adId = Long.parseLong(adId);
        return this;
    }

    /**
     * Get adId
     *
     * @return adId
     **/
    @ApiModelProperty(value = "")

    public Long getAdId() {
        return adId;
    }

    public void setAdId( String adId) {
        this.adId = Long.parseLong(adId);
    }

    public ActionDTO status(Status status) {
        this.previousStatus = status;
        return this;
    }

    /**
     * Get previousStatus
     *
     * @return previousStatus
     **/
    @ApiModelProperty(value = "")

    @Valid
    public Status getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(Status previousStatus) {
        this.previousStatus = previousStatus;
    }

    public ActionDTO newBid(Double newBid) {
        this.newBid = newBid;
        return this;
    }

    /**
     * Get newBid
     *
     * @return newBid
     **/
    @ApiModelProperty(value = "", required = false)

    public Double getNewBid() {
        return newBid;
    }

    public void setNewBid(Double newBid) {
        this.newBid = newBid;
    }

    public ActionDTO previousBid(Double previousBid) {
        this.previousBid = previousBid;
        return this;
    }

    /**
     * Get previousBid
     *
     * @return previousBid
     **/
    @ApiModelProperty(value = "")

    public Double getPreviousBid() {
        return previousBid;
    }

    public void setPreviousBid(Double previousBid) {
        this.previousBid = previousBid;
    }


    public ActionDTO sku(String sku) {
        this.sku = sku;
        return this;
    }

    /**
     * Get sku
     *
     * @return sku
     **/
    @ApiModelProperty(value = "")

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public ActionDTO asin(String asin) {
        this.asin = asin;
        return this;
    }

    /**
     * Get asin
     *
     * @return asin
     **/
    @ApiModelProperty(value = "")

    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }

    public ActionDTO keywordText(String keywordText) {
        this.keywordText = keywordText;
        return this;
    }

    /**
     * Get keywordText
     *
     * @return keywordText
     **/
    @ApiModelProperty(value = "")

    public String getKeywordText() {
        return keywordText;
    }

    public void setKeywordText(String keywordText) {
        this.keywordText = keywordText;
    }

    public ActionDTO matchType(MatchType matchType) {
        this.matchType = matchType;
        return this;
    }

    /**
     * Get matchType
     *
     * @return matchType
     **/
    @ApiModelProperty(value = "")

    @Valid
    public MatchType getMatchType() {
        return matchType;
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType;
    }


    public ActionDTO campaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    /**
     * Get campaignType
     *
     * @return campaignType
     **/
    @ApiModelProperty(value = "")

    @Valid
    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }



    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ActionDTO action = (ActionDTO) o;
        return Objects.equals(this.clientId, action.clientId) &&
                Objects.equals(this.campaignId, action.campaignId) &&
                Objects.equals(this.previousBudget, action.previousBudget) &&
                Objects.equals(this.newBudget, action.newBudget) &&
                Objects.equals(this.newStatus, action.newStatus) &&
                Objects.equals(this.profileId, action.profileId) &&
                Objects.equals(this.adgroupId, action.adgroupId) &&
                Objects.equals(this.keywordId, action.keywordId) &&
                Objects.equals(this.adId, action.adId) &&
                Objects.equals(this.previousStatus, action.previousStatus) &&
                Objects.equals(this.newBid, action.newBid) &&
                Objects.equals(this.previousBid, action.previousBid) &&
                Objects.equals(this.sku, action.sku) &&
                Objects.equals(this.asin, action.asin) &&
                Objects.equals(this.keywordText, action.keywordText) &&
                Objects.equals(this.matchType, action.matchType) &&
                Objects.equals(this.campaignType, action.campaignType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, profileId, campaignId, adgroupId, newBudget, previousBudget,
                newStatus, keywordId, adId, previousStatus, newBid, previousBid, sku, asin, keywordText, matchType, campaignType);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ActionDTO {\n");

        sb.append("    clientId: ").append(toIndentedString(clientId)).append("\n");
        sb.append("    profileId: ").append(toIndentedString(profileId)).append("\n");
        sb.append("    adgroupId: ").append(toIndentedString(adgroupId)).append("\n");
        sb.append("    keywordId: ").append(toIndentedString(keywordId)).append("\n");
        sb.append("    campaignId: ").append(toIndentedString(campaignId)).append("\n");
        sb.append("    previousBudget: ").append(toIndentedString(previousBudget)).append("\n");
        sb.append("    newBudget: ").append(toIndentedString(newBudget)).append("\n");
        sb.append("    newStatus: ").append(toIndentedString(newStatus)).append("\n");
        sb.append("    adId: ").append(toIndentedString(adId)).append("\n");
        sb.append("    previousStatus: ").append(toIndentedString(previousStatus)).append("\n");
        sb.append("    newBid: ").append(toIndentedString(newBid)).append("\n");
        sb.append("    previousBid: ").append(toIndentedString(previousBid)).append("\n");
        sb.append("    sku: ").append(toIndentedString(sku)).append("\n");
        sb.append("    asin: ").append(toIndentedString(asin)).append("\n");
        sb.append("    keywordText: ").append(toIndentedString(keywordText)).append("\n");
        sb.append("    matchType: ").append(toIndentedString(matchType)).append("\n");
        sb.append("    campaignType: ").append(toIndentedString(campaignType)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    protected String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

