package com.boomerang.brands.clientsdk.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class ActionLoggerReq  {
    @JsonProperty("actionType")
    private ActionType actionType = null;

    @JsonProperty("actionPayload")
    private ActionDTO actionDTO = null;

    @JsonProperty("viewPayload")
    private Map<String, Object> viewPayload = null;

    @JsonProperty("actionSource")
    private Map<String, Object> actionSource = null;

    public ActionLoggerReq actionType(ActionType actionType) {
        this.actionType = actionType;
        return this;
    }

    /**
     * Get actionType
     *
     * @return actionType
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public ActionLoggerReq actionPayload(ActionDTO actionDTO) {
        this.actionDTO = actionDTO;
        return this;
    }

    /**
     * Get actionDTO
     *
     * @return actionDTO
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public ActionDTO getActionDTO() {
        return actionDTO;
    }

    public void setActionDTO(ActionDTO actionDTO) {
        this.actionDTO = actionDTO;
    }

    public ActionLoggerReq viewPayload(final Map<String, Object> viewPayload) {
        this.viewPayload = viewPayload;
        return this;
    }

    /**
     * Get viewPayload
     *
     * @return viewPayload
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public Map<String, Object> getActionSource() {
        return actionSource;
    }

    public void setActionSource(final Map<String, Object> actionSource) {
        this.actionSource = actionSource;
    }

    public ActionLoggerReq actionSource(final Map<String, Object> actionSource) {
        this.actionSource = actionSource;
        return this;
    }

    /**
     * Get viewType
     *
     * @return viewType
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public Map<String, Object> getViewPayload() {
        return viewPayload;
    }

    public void setViewPayload(final Map<String, Object> viewPayload) {
        this.viewPayload = viewPayload;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ActionLoggerReq actionLoggerReq = (ActionLoggerReq) o;
        return Objects.equals(this.actionType, actionLoggerReq.actionType) &&
                Objects.equals(this.actionDTO, actionLoggerReq.actionDTO) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(actionType, actionDTO);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ActionLoggerReq {\n");

        sb.append("        actionType: ").append(toIndentedString(actionType)).append("\n");
        sb.append("        actionDTO: ").append(toIndentedString(actionDTO)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n        ");
    }
}

