package com.boomerang.brands.clientsdk.model;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum Widget {
    keyword("keyword"),
    campaign("campaign"),
    searchTerm("searchTerm"),
    skus("skus"),
    amsSkus("amsSkus");

    private final String val;

    Widget( final String widgetText){
        val = widgetText;
    }

    @Override
    public String toString() {
        return val;
    }

    @JsonCreator
    public static Widget fromValue(String text) {
        return valueOf(text);
    }

}
