package com.boomerang.brands.clientsdk;

import com.boomerang.brands.clientsdk.api.ActionLoggingApi;
import com.boomerang.brands.clientsdk.api.ActionLoggingApiWrapper;
import com.boomerang.brands.clientsdk.api.req.WorkLogRetrieveActionsReq;
import com.boomerang.brands.clientsdk.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;


import javax.ws.rs.core.MediaType;
import java.util.*;

public class Main {

//
//    public static void main(String[] args){
//
//        JacksonJsonProvider provider = new JacksonJsonProvider();
//
//        ActionLoggingApi api;
//
//        api = JAXRSClientFactory.create("http://brands-service-beta.rboomerang.com:8080",
//                ActionLoggingApi.class, Collections.singletonList(
//                        new JacksonJaxbJsonProvider(new ObjectMapper(), new Annotations[] {Annotations.JACKSON})
//                ));
//        org.apache.cxf.jaxrs.client.Client client = WebClient.client(api);
//
//        WebClient.getConfig(client).getHttpConduit()
//                .getClient().setReceiveTimeout( 3000000L );
//        WebClient.client( client ).accept( MediaType.APPLICATION_JSON_TYPE );
//        WebClient.client( client ).type( MediaType.APPLICATION_JSON_TYPE );
//
//
//        List<ActionLoggerReq> body = new ArrayList<>();
//
//        final ActionLoggerReq req = new ActionLoggerReq();
//
//        final ActionDTO dto = new ActionDTO();
//        dto.clientId(549);
//        dto.profileId("3723912496707446");
//        dto.campaignId("199061807549629");
//        dto.newBudget(1005.99);
//        dto.previousBudget(1117.77);
//
//        final Map<String, Object> viewPayload = new HashMap<>();
//        viewPayload.put("campaignId", 199061807549629L);
//        viewPayload.put("newBudget", 1005.99);
//        viewPayload.put("previousBudget", 1117.77);
//        viewPayload.put("name", 199061807549629L);
//        viewPayload.put("entityId", 199061807549629L);
//
//        final Map<String, Object> actionSource = new HashMap<>();
//
//        req.setActionType(ActionType.CAMPAIGNBUDGETCHANGE);
//        req.setActionDTO(dto);
//        req.setViewPayload(viewPayload);
//        req.setActionSource(actionSource);
//
//        body.add(req);
//        ActionLoggingApiWrapper wrapper = new ActionLoggingApiWrapper("http://brands-service-beta.rboomerang.com:8080");
//
//
//        List<ActionLoggerResp> responses = wrapper.getActionsTaken("hiball", 301, "user",
//                new WorkLogRetrieveActionsReq(Widget.campaign, "199061807549629"));
//
//        System.out.println("ray232" + responses.size());
//
//            System.out.println("ray232" + responses.get(0).getActionId());
//        // TODO: test validations
//
//    }
}
