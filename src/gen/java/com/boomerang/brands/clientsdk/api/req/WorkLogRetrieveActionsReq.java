package com.boomerang.brands.clientsdk.api.req;

import com.boomerang.brands.clientsdk.model.ActionStatus;
import com.boomerang.brands.clientsdk.model.ActionType;
import com.boomerang.brands.clientsdk.model.Widget;


public final class WorkLogRetrieveActionsReq {

    public enum TimeRange { START, END};
    private Widget widget;
    private ActionStatus actionStatus;
    private String primaryKey;

    private Long actionId;

    public Widget getWidget() {
        return widget;
    }

    public ActionStatus getActionStatus() {
        return actionStatus;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public Long getActionId() {
        return actionId;
    }

    private ActionType actionType;
    private String startTime;
    private String endTime;

    public WorkLogRetrieveActionsReq(){}

    public WorkLogRetrieveActionsReq( final Widget w, final String pk){
        widget = w;
        primaryKey = pk;
    }

    public WorkLogRetrieveActionsReq( final Widget w, final String pk, final Long action){
        widget = w;
        primaryKey = pk;
        actionId = action;
    }

    public WorkLogRetrieveActionsReq( final ActionType a){
        actionType = a;
    }

    public WorkLogRetrieveActionsReq( final ActionStatus a){
        actionStatus = a;
    }
    public WorkLogRetrieveActionsReq( final Widget w){
        widget = w;
    }
    public WorkLogRetrieveActionsReq( final String t, final TimeRange tr){
        if( TimeRange.START.equals(tr)){
            startTime = t;
        } else if (TimeRange.END.equals(tr)){
            endTime = t;
        }
    }
    public WorkLogRetrieveActionsReq( final String st, final String et){
        startTime = st;
        endTime = et;
    }

}
