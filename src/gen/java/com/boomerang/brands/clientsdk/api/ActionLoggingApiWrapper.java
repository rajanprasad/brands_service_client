package com.boomerang.brands.clientsdk.api;

import com.boomerang.brands.api.response.ApiResponse;
import com.boomerang.brands.clientsdk.api.req.WorkLogRetrieveActionsReq;
import com.boomerang.brands.clientsdk.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ActionLoggingApiWrapper {

    private final ActionLoggingApi api;

    public ActionLoggingApiWrapper( final String baseUrl){
        this(baseUrl, 3000000L);
    }

    public ActionLoggingApiWrapper( final String baseUrl, final long timeOut){

        api = JAXRSClientFactory.create(baseUrl, ActionLoggingApi.class, Collections.singletonList(
                new JacksonJaxbJsonProvider(new ObjectMapper(), new Annotations[] {Annotations.JACKSON})
        ));
        org.apache.cxf.jaxrs.client.Client client = WebClient.client(api);

        WebClient.getConfig(client).getHttpConduit()
                .getClient().setReceiveTimeout( timeOut );
        WebClient.client( client ).accept( MediaType.APPLICATION_JSON_TYPE );
        WebClient.client( client ).type( MediaType.APPLICATION_JSON_TYPE );
    }

    @NotNull
    public List<ActionLoggerResp> executeAction( final String client, final int clientId, final String username,
                                          final List<ActionLoggerReq> requests){
        final ApiResponse<ArrayList<ActionLoggerResp>> response
                = api.executeAction( client, clientId, username, requests);
        if( null == response || null == response.getResponse())
            throw new IllegalStateException("Executing the action failed.");
        else {
            return response.getResponse();
        }
    }

    @NotNull
    public ArrayList<ActionLoggerResp> executeBulkAction(final String client, final int clientId, final String username,
                                                         final List<ActionLoggerReq> requests){
        final ApiResponse<ArrayList<ActionLoggerResp>> response
                = api.executeActionsBulk( client, clientId, username, requests);
        if( null == response || null == response.getResponse())
            throw new IllegalStateException("Executing the action failed.");
        else {
            return response.getResponse();
        }
    }

    @NotNull
    public List<ActionLoggerResp> getActionsTaken(String client, Integer clientId,
                                                  String username,
                                                  final WorkLogRetrieveActionsReq req){
        final ApiResponse<ArrayList<ActionLoggerResp>> response
                = api.getActionsTaken( client, clientId, username, req.getWidget(),
                req.getActionStatus(), req.getActionId(), req.getPrimaryKey(),
                req.getActionType(), req.getStartTime(), req.getEndTime());

        if( null == response || null == response.getResponse())
            throw new IllegalStateException("Retrieving the actions failed.");
        else {
            return response.getResponse();
        }
    }
}
