package com.boomerang.brands.clientsdk.api;

import com.boomerang.brands.clientsdk.model.*;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import javax.ws.rs.*;

import io.swagger.annotations.*;

/**
 * Swagger Petstore
 *
 * <p>This is a sample Petstore server.  You can find  out more about Swagger at  [http://swagger.io](http://swagger.io) or on  [irc.freenode.net, #swagger](http://swagger.io/irc/). 
 *
 */
@Api(value = "/", description = "")
public interface ActionLoggingApi {

    /**
     * Execute an action
     *
     * 
     *
     */
    @POST
    @Path("/worklog")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Execute an action", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Action posted for execution", response = ActionLoggerResp.class, responseContainer = "List"),
        @ApiResponse(code = 405, message = "Invalid input") })
    public com.boomerang.brands.api.response.ApiResponse<ArrayList<ActionLoggerResp>> executeAction(
            @HeaderParam("client") String client,
            @HeaderParam("clientId") Integer clientId,
            @HeaderParam("username") String username,
            @ApiParam(value = "Array of actions executed" ,required=true )
            @Valid @Size(max = 300) List<ActionLoggerReq> body);


    /**
     * Execute an action
     *
     *
     *
     */
    @POST
    @Path("/worklogBulk")
    @ApiOperation(
            value = "Execute an action",
            nickname = "executeAction",
            notes = "",
            response = ActionLoggerResp.class,
            responseContainer = "List",
            tags = {}
    )
    @ApiResponses(
            value = {
                    @io.swagger.annotations.ApiResponse(
                            code = 200,
                            message = "Action posted for execution",
                            response = ActionLoggerResp.class,
                            responseContainer = "List"

                    ),
                    @io.swagger.annotations.ApiResponse(code = 405, message = "Invalid input")
            }
    )
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    com.boomerang.brands.api.response.ApiResponse<ArrayList<ActionLoggerResp>> executeActionsBulk(
            @HeaderParam("client") String client,
            @HeaderParam("clientId") Integer clientId,
            @HeaderParam("username") String username,
            @ApiParam(value = "Array of actions executed" ,required=true )
            @Valid @Size(max = 300) List<ActionLoggerReq> body
    );



    /**
     * Retrieve all actions taken which satisfy the given filters.
     *
     * Get actions details
     *
     */
    @GET
    @Path("/worklog")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Retrieve all actions taken which satisfy the given filters.",
            tags={  },
            response = ActionLoggerResp.class,
            responseContainer = "List"
    )
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = ActionLoggerResp.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Invalid ID supplied"),
        @ApiResponse(code = 404, message = "Resource not found") })
    public com.boomerang.brands.api.response.ApiResponse<ArrayList<ActionLoggerResp>> getActionsTaken(
            @HeaderParam("client") String client,
            @HeaderParam("clientId") Integer clientId,
            @HeaderParam("username") String username,
            @ApiParam(value = "Retrieve the actions taken on this widget")
            @QueryParam("widget") Widget widget,
            @ApiParam(value = "Retrieve the actions in this state")
            @QueryParam("actionStatus") ActionStatus actionStatus,
            @ApiParam(value = "Retrieve the actions associated with this actionId")
            @QueryParam("actionId")Long actionId,
            @ApiParam(value = "Retrieve the actions associated with this key")
            @QueryParam("primaryKey")String primaryKey,
            @ApiParam(value = "Retrieve the actions taken of this type")
            @QueryParam("actionType") ActionType actionType,
            @ApiParam(value = "Retrieve the actions taken on or after this time")
            @QueryParam("startTime")String startTime,
            @ApiParam(value = "Retrieve the actions taken on or before this time")
            @QueryParam("endTime")String endTime);
}

